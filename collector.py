import os
import datetime
import shutil

# DON'T REMOVE THIS THINGS.
whitelist = ["torrenty", "Supercard", "EtreCheck.app", "snes", "holandia"]

# SET CURRENT DATE, DIRECTORY TO THE FOLDER YOU WANT TO CLEAN.
folder_path = '/Users/Piotras/Downloads/'
current_date = datetime.datetime.today()
time_limit = 30

# ITERATE THROUGH ALL THE THINGS IN DIRECTORY AND CHECK IF THEY EXIST
# FOR MORE THAN 30 DAYS. IF IT'S TRUE THEN DELETE.
for file in os.listdir(folder_path):
    if file not in whitelist:
        file_date = datetime.datetime.fromtimestamp(os.path.getmtime(folder_path + file))
        file_lifecycle = current_date - file_date
        if file_lifecycle.days > time_limit:
            if os.path.isdir(folder_path + file):
                shutil.rmtree(folder_path + file)
            elif os.path.isfile(folder_path + file):
                os.remove(folder_path + file)