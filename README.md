# Garbage collector

Last modified: 2018-03-30

## Introduction
I always got a problem with my downloads folder. I used to download a lot of things which I forget to delete. After a short period of time I get a lot of mess in which I can not find myself. Because of that I have created my own garbage collector for downloads directory.


### Before:
![Optional Text](./screenshoots/Before.png)


### After:
![Optional Text](./screenshoots/After.png)


## Requirements
**Python 3.6**

**5 minutes of free time**

## Usage
**It depends** how you want to use it, because there are two ways.

### First
**Just launch script from your command line.**

e.g. python3 collector.py
![Optional Text](./screenshoots/Terminal.png)

It will just clean your directory.


### Second (more cool)
**Automate script usage.**

For macOS launch Automator and choose application.
On the left column choose "Utilities" and then drag "Run Shell Script"
into the workspace area on the right. As shell choose "/bin/bash".
Right now you need to tell automator what should he do. In my case
I needed to add path where my python3 is and then path to collector.py file.
Remember to save!

![Optional Text](./screenshoots/Automator.png)


In the next step launch Calendar app and create a new event. Modify it so it will repeat it-self as many times as you want.


![Optional Text](./screenshoots/Calendar.png)

All this means that every day, (in my case) until december 31 the script will be automatically launched. Don't need to worry about thrash anymore :)

## How to modify
There are only three things that you would like to change:
*  **folder_path** variable to suit you're directory.
*  **time_limit** variable to adjust after how many days file should be deleted.
*  **whitelist** if you want to keep some things safe
